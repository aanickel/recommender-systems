recommender-systems
==============================

The goal of this repo is to accurately predict the rating a user will give a given book.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>


## UCSD Book Graph Statistics

| Collection 	|   Count   	|
|:----------:	|:---------:	|
|   # book   	| 2,360,655 	|
|   # work   	| 1,521,962 	|
|  # author  	|  829,529  	|
|  # series  	|  400,390  	|


|               	|   children   	| comics_graphic 	| fantasy_paranormal 	| history_biography 	| mystery_thriller_crime 	|    poetry   	|    romance   	|  young_adult 	|
|:-------------:	|:------------:	|:--------------:	|:------------------:	|:-----------------:	|:----------------------:	|:-----------:	|:------------:	|:------------:	|
|     # book    	|   124,082  	|    89,411    	    |      258,585     	    |     302,935     	|        219,235       	|   36,514  	|   335,449  	|   93,398   	|
|     # user    	|   542,145  	|    342,415   	    |      726,932     	    |     761,215     	|        676,075       	|  377,799  	|   655,454  	|   644,686  	|
|    # shelve   	| 10,059,349 	|   7,347,630  	    |    55,397,550    	    |    31,479,229   	|      24,799,896      	| 2,734,350 	| 42,792,856 	| 34,919,254 	|
|     # read    	|  6,626,989 	|   4,764,133  	    |    27,904,041    	    |    13,436,575   	|      12,524,984      	| 1,313,610 	| 21,174,642 	| 15,722,749 	|
|     # rate    	|  6,384,470 	|   4,514,094  	    |    26,193,771    	    |    12,379,895   	|      11,715,518      	| 1,229,059 	| 19,701,197 	| 14,731,908 	|
|    # review   	|   736,682  	|    544,371   	    |     3,444,043    	    |    2,074,497    	|       1,856,053      	|  155,414  	|  3,585,643 	|  2,405,359 	|
| # shelve/book 	|     81.07    	|      82.18     	|       214.23       	|       103.91      |         113.12        |    74.88    	|    127.57    	|    373.88    	|
| # shelve/user 	|     18.55    	|      21.46     	|        76.21       	|       41.35       |          36.68        |     7.24    	|     65.29    	|     54.16    	|
|  # read/book  	|     53.41    	|      53.28     	|       107.91       	|       44.35      	|          57.13        |    35.98    	|     63.12    	|    168.34    	|
|  # read/user  	|     12.22    	|      13.91     	|        38.39       	|       17.65       |          18.53        |     3.48    	|     32.31    	|     24.39    	|
|  # rate/book  	|     51.45    	|      50.49     	|        101.3       	|       40.87       |          53.44        |    33.66    	|     58.73    	|    157.73    	|
|  # rate/user  	|     11.78    	|      13.18     	|        36.03       	|       16.26       |          17.33        |     3.25    	|     30.06    	|     22.85    	|
| # review/book 	|     5.94     	|      6.09      	|        13.32       	|        6.85       |          8.47         |     4.26    	|     10.69    	|     25.75    	|
| # review/user 	|     1.36     	|      1.59      	|        4.74        	|        2.73       |          2.75         |     0.41    	|     5.47     	|     3.73     	|


| Collection 	|    Count    	|
|:----------:	|:-----------:	|
|  # shelve  	| 228,648,342 	|
|   # read   	| 112,131,203 	|
|   # rate   	| 104,551,549 	|
|  # review  	|  16,219,149 	|


### Data Structures

#### Works Json

```yaml
{
    "books_count":"2",
    "reviews_count":"33",
    "original_publication_month":"",
    "default_description_language_code":"",
    "text_reviews_count":"4",
    "best_book_id":"378460",
    "original_publication_year":"",
    "original_title":"The Wanting of Levine",
    "rating_dist":"5:7|4:4|3:2|2:0|1:0|total:13",
    "default_chaptering_book_id":"",
    "original_publication_day":"",
    "original_language_id":"",
    "ratings_count":"13",
    "media_type":"",
    "ratings_sum":"57",
    "work_id":"368291"
}
```

#### Book Json

```yaml
{
    "isbn":"1591935857",
    "text_reviews_count":"4",
    "series":[

    ],
    "country_code":"US",
    "language_code":"",
    "popular_shelves":[
        {
            "count":"2",
            "name":"picture-books"
        },
        {
            "count":"2",
            "name":"ducks"
        },
        {
            "count":"1",
            "name":"online-reading-in-the-stacks"
        },
        {
            "count":"1",
            "name":"nature"
        },
        {
            "count":"1",
            "name":"children-books"
        },
        {
            "count":"1",
            "name":"animal-books"
        },
        {
            "count":"1",
            "name":"19709"
        },
        {
            "count":"1",
            "name":"17909-books"
        },
        {
            "count":"1",
            "name":"to-read"
        },
        {
            "count":"1",
            "name":"outreach-books"
        },
        {
            "count":"1",
            "name":"books-for-teaching"
        },
        {
            "count":"1",
            "name":"picture-books-read"
        },
        {
            "count":"1",
            "name":"photographs"
        },
        {
            "count":"1",
            "name":"birds"
        },
        {
            "count":"1",
            "name":"ald_neighborhood-animals"
        }
    ],
    "asin":"",
    "is_ebook":"false",
    "average_rating":"4.29",
    "kindle_asin":"",
    "similar_books":[

    ],
    "description":"",
    "format":"Hardcover",
    "link":"https://www.goodreads.com/book/show/27036533-jump-little-wood-ducks",
    "authors":[
        {
            "author_id":"13195",
            "role":""
        },
        {
            "author_id":"30853",
            "role":"Photographs"
        }
    ],
    "publisher":"Adventurekeen",
    "num_pages":"36",
    "publication_day":"24",
    "isbn13":"9781591935858",
    "publication_month":"2",
    "edition_information":"",
    "publication_year":"2016",
    "url":"https://www.goodreads.com/book/show/27036533-jump-little-wood-ducks",
    "image_url":"https://images.gr-assets.com/books/1473603845m/27036533.jpg",
    "book_id":"27036533",
    "ratings_count":"7",
    "work_id":"47077776",
    "title":"Jump, Little Wood Ducks",
    "title_without_series":"Jump, Little Wood Ducks"
}
```

#### Author Json

```yaml
{
    "average_rating":"3.51",
    "author_id":"2943855",
    "text_reviews_count":"634",
    "name":"Kat Menschik",
    "ratings_count":"4599"
}
```

#### Series Json

```yaml
{
    "numbered":"true",
    "note":"",
    "description":"War Stories was a comic book series written by Garth Ennis.",
    "title":"War Stories",
    "series_works_count":"5",
    "series_id":"834955",
    "primary_work_count":"4"
}
```

#### Review JSON

```yaml
{
    "user_id":"8842281e1d1347389f2ab93d60773d4d",
    "book_id":"18245960",
    "review_id":"dfdbb7b0eb5a7e4c26d59a937e2e5feb",
    "rating":5,
    "review_text":"This is a special book. It started slow for about the first third, then in the middle third it started to get interesting, then the last third blew my mind. This is what I love about good science fiction - it pushes your thinking about where things can go. \n It is a 2015 Hugo winner, and translated from its original Chinese, which made it interesting in just a different way from most things I\\'ve read. For instance the intermixing of Chinese revolutionary history - how they kept accusing people of being \"reactionaries\", etc. \n It is a book about science, and aliens. The science described in the book is impressive - its a book grounded in physics and pretty accurate as far as I could tell. Though when it got to folding protons into 8 dimensions I think he was just making stuff up - interesting to think about though. \n But what would happen if our SETI stations received a message - if we found someone was out there - and the person monitoring and answering the signal on our side was disillusioned? That part of the book was a bit dark - I would like to think human reaction to discovering alien civilization that is hostile would be more like Enders Game where we would band together. \n I did like how the book unveiled the Trisolaran culture through the game. It was a smart way to build empathy with them and also understand what they\\'ve gone through across so many centuries. And who know a 3 body problem was an unsolvable math problem? But I still don\\'t get who made the game - maybe that will come in the next book. \n I loved this quote: \n \"In the long history of scientific progress, how many protons have been smashed apart in accelerators by physicists? How many neutrons and electrons? Probably no fewer than a hundred million. Every collision was probably the end of the civilizations and intelligences in a microcosmos. In fact, even in nature, the destruction of universes must be happening at every second--for example, through the decay of neutrons. Also, a high-energy cosmic ray entering the atmosphere may destroy thousands of such miniature universes....\"",
    "date_added":"Sun Jul 30 07:44:10 -0700 2017",
    "date_updated":"Wed Aug 30 00:00:26 -0700 2017",
    "read_at":"Sat Aug 26 12:05:52 -0700 2017",
    "started_at":"Tue Aug 15 13:23:18 -0700 2017",
    "n_votes":28,
    "n_comments":1
}
```

#### Spoiler JSON

```yaml
{
    "user_id":"01ec1a320ffded6b2dd47833f2c8e4fb",
    "timestamp":"2016-01-14",
    "review_sentences":[
        [
            0,
            "3.5 - 4 Stars"
        ],
        [
            0,
            "This is the very sexy and very sweet sequel to the short story, Hearts in Darkness."
        ],
        [
            0,
            "This book picks up shortly after Caden and Makenna first met in the darkened elevator."
        ],
        [
            0,
            "They have developed an almost-perfect relationship - and this book is verra verra steamy - but Caden has severe PTSD due to a very shocking incident in his past that has completely traumatized him."
        ],
        [
            0,
            "This is one of those stories where the hero doesn't think he is good enough for the heroine - she is smart, kind, beautiful, has a wonderful family, etc."
        ],
        [
            0,
            "- so the hero must distance himself from her \"for her own good.\""
        ],
        [
            0,
            "Makenna loves Caden but realizes that he is gun-shy and she is scared to make her true feelings known."
        ],
        [
            0,
            "When she does, all hell breaks loose."
        ],
        [
            0,
            "This is a lovely, steamy story with lots of angst and a beautiful HEA."
        ],
        [
            0,
            "If you are a fan of Laura Kaye's writing, then you will love this book."
        ],
        [
            0,
            "(ARC provided in return for an honest review.)"
        ]
    ],
    "rating":4,
    "has_spoiler":false,
    "book_id":"25396316",
    "review_id":"27f0566f9f72429e4d25bbbf584434b4"
}
```

#### Raw Spoiler

```yaml
{
    "user_id":"8842281e1d1347389f2ab93d60773d4d",
    "book_id":"28684704",
    "review_id":"2ede853b14dc4583f96cf5d120af636f",
    "rating":3,
    "review_text":"A fun, fast paced science fiction thriller. I read it in 2 nights and couldn\\'t put it down. The book is about the quantum theory of many worlds which states that all decisions we make throughout our lives basically create branches, and that each possible path through the decision tree can be thought of as a parallel world. And in this book, someone invents a way to switch between these worlds. This was nicely alluded to/foreshadowed in this quote: \n \"I think about all the choices we\\'ve made that created this moment. Us sitting here together at this beautiful table. Then I think of all the possible events that could have stopped this moment from ever happening, and it all feels, I don\\'t know...\" \"What?\" \"So fragile.\" Now he becomes thoughtful for a moment. He says finally, \"It\\'s terrifying when you consider that every thought we have, every choice we could possibly make, branches into a new world.\" \n (view spoiler)[This book can\\'t be discussed without spoilers. It is a book about choice and regret. Ever regret not chasing the girl of your dreams so you can focus on your career? Well Jason2 made that choice and then did regret it. Clearly the author is trying to tell us to optimize for happiness - to be that second rate physics teacher at a community college if it means you can have a happy life. I\\'m being snarky because while there is certainly something to that, you also have to have meaning in your life that comes from within. I thought the book was a little shallow on this dimension. In fact, all the characters were fairly shallow. Daniela was the perfect wife. Ryan the perfect antithesis of Jason. Amanda the perfect loyal traveling companion, etc. This, plus the fact that the book was weak on the science are what led me to take a few stars off - but I\\'d still read it again if I could go back in time - was a very fun and engaging read. \n If you want to really minimize regret, you have to live your life to avoid it in the first place. Regret can\\'t be hacked, which is kind of the point of the book. My favorite book about regret is Remains of the Day. I do really like the visualization of the decision tree though - that is a powerful concept. \n \"Every moment, every breath, contains a choice. But life is imperfect. We make the wrong choices. So we end up living in a state of perpetual regret, and is there anything worse? I built something that could actually eradicate regret. Let you find worlds where you made the right choice.\" Daniela says, \"Life doesn\\'t work that way. You live with your choices and learn. You don\\'t cheat the system.\" \n (hide spoiler)]",
    "date_added":"Tue Nov 15 11:29:22 -0800 2016",
    "date_updated":"Mon Mar 20 23:40:27 -0700 2017",
    "read_at":"Sat Mar 18 23:22:42 -0700 2017",
    "started_at":"Fri Mar 17 23:45:40 -0700 2017",
    "n_votes":22,
    "n_comments":0
}
```

### Initial Observations (Interactions CSV)

Noticed that converting string datetime to datetype datatype led to the following issues:

total rows with null value in date_added_timestamp: 1
- number of null ("") values from date_added: 0
- errors: 1
    - review_id: 189a1c9356bd49a733e5b350b55c7c76 -> year was input as 1092

total rows with null value in date_updated_timestamp: 0
- number of null ("") values from date_updated: 0

total rows with null value in read_at_timestamp: 2,767,450
- number of null ("") values from read_at: 2,766,813
- errors: 637
    - errors due to non-real year
    - number of errors shared with started_at_timestamp: 600
    - number of errors unique to read_at_timestamp: 37

total rows with null value in started_at_timestamp == 6,713,159
- number of null ("") values from started_at: 6,712,475
- errors: 684
    - errors due to non-real year
    - number of errors shared with read_at_timestamp: 600
    - number of errors unique to started_at_timestamp: 84

total rows with a null value in date_added_timestamp, date_updated_timestamp, read_at_timestamp, started_at_timestamp == 6885052

rows where timestamp updated occurs before started: 10904
