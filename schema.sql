-- schema.sql
-- Since we might run the import many times we'll drop if exists
-- Make sure we're using our `blog` database
\c postgres

DROP DATABASE IF EXISTS bookgraph;

CREATE DATABASE bookgraph;

\c bookgraph;

-- User Table
CREATE TABLE IF NOT EXISTS users (
  user_id INTEGER PRIMARY KEY,
  user_uuid SERIAL
);

-- Author Table
CREATE TABLE IF NOT EXISTS authors (
  average_rating FLOAT,
  author_id INTEGER PRIMARY KEY,
  text_review_count INTEGER,
  name TEXT,
  ratings_count INTEGER
);

-- Works Table
CREATE TABLE IF NOT EXISTS works (
  work_id INTEGER PRIMARY KEY,
  book_count INTEGER,
  reviews_count INTEGER,
  default_description_language_code TEXT,
  text_reviews_count INTEGER,
  original_publication_year INTEGER,
  original_title TEXT,
  five_rating INTEGER,
  four_rating INTEGER,
  three_rating INTEGER,
  two_rating INTEGER,
  one_rating INTEGER,
  default_chaptering_book_id TEXT,
  original_publication_day INTEGER,
  original_language_id TEXT,
  ratings_count INTEGER,
  media_type TEXT,
  ratings_sum INTEGER
);

-- Series Table
CREATE TABLE IF NOT EXISTS series (
  series_id INTEGER PRIMARY KEY,
  numbered BOOLEAN,
  note TEXT,
  description TEXT,
  title TEXT,
  series_work_count INTEGER,
  primary_work_count INTEGER
);

-- Book Table
CREATE TABLE IF NOT EXISTS books (
  book_id INTEGER PRIMARY KEY,
  isbn TEXT,
  text_reviews_count INTEGER,
  series TEXT,
  country_code TEXT,
  language_code TEXT,
  asin TEXT,
  is_ebook BOOLEAN,
  average_rating FLOAT,
  kindle_asin TEXT,
  description TEXT,
  format TEXT,
  link TEXT,
  publisher TEXT,
  num_pages INTEGER,
  publication_day INTEGER,
  isbn_13 TEXT,
  publication_month INTEGER,
  edition_information TEXT,
  publication_year INTEGER,
  url TEXT,
  image_url TEXT,
  rating_count INTEGER,
  work_id INTEGER REFERENCES works(work_id),
  title TEXT,
  title_without_series TEXT
);

ALTER TABLE works
ADD COLUMN best_book_id INTEGER REFERENCES books(book_id);

-- Interactions Table
DROP TABLE IF EXISTS interactions;
CREATE TABLE interactions (
  user_id INTEGER,
  book_id INTEGER,
  is_read BOOLEAN,
  rating INTEGER,
  is_reviewed BOOLEAN
);
-- -- Interactions Table
-- CREATE TABLE IF NOT EXISTS interactions (
--   user_id INTEGER REFERENCES users(user_id),
--   book_id INTEGER REFERENCES books(book_id),
--   review_id SERIAL PRIMARY KEY,
--   is_read BOOLEAN,
--   rating INTEGER,
--   review_text_incomplete TEXT,
--   date_added TIMESTAMPTZ,
--   date_updated TIMESTAMPTZ,
--   read_at TIMESTAMPTZ,
--   started_at TIMESTAMPTZ
-- );

-- Genre Table
-- TODO: Implement the Genre table.
-- CREATE TABLE IF NOT EXISTS genre (
--   id SERIAL PRIMARY KEY,
--   user_id INTEGER REFERENCES users(user_id),
--   title VARCHAR,
--   content TEXT,
--   image VARCHAR,
--   date DATE DEFAULT CURRENT_DATE
-- );

-- Reviews Table
-- CREATE TABLE IF NOT EXISTS reviews (
--   review_id SERIAL PRIMARY KEY REFERENCES interactions(review_id),
--   user_id INTEGER REFERENCES users(user_id),
--   book_id INTEGER REFERENCES books(book_id),
--   rating INTEGER,
--   review_text TEXT,
--   date_added TIMESTAMPTZ,
--   date_updated TIMESTAMPTZ,
--   read_at TIMESTAMPTZ,
--   started_at TIMESTAMPTZ,
--   n_votes INTEGER,
--   CHECK (n_votes >= 0),
--   n_comments INTEGER,
--   CHECK (n_comments >= 0)
-- );

COPY interactions
FROM '/data/goodreads_interactions.csv'
WITH (format csv, delimiter ',', header);
