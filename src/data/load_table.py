from collections import namedtuple
import json
import gzip
from io import StringIO
import logging
import os
from pathlib import Path
import sys

import pandas as pd
import psycopg2
from tqdm import tqdm

PostgresCredentials = namedtuple(
    "PostgresCredentials", ["host", "database", "user", "password", "port"]
)

book_keys = [
    "book_id",
    "isbn",
    "text_reviews_count",
    "series",
    "country_code",
    "language_code",
    "asin",
    "is_ebook",
    "average_rating",
    "kindle_asin",
    "description",
    "format",
    "link",
    "publisher",
    "num_pages",
    "publication_day",
    "isbn13",
    "publication_month",
    "edition_information",
    "publication_year",
    "url",
    "image_url",
    "ratings_count",
    "work_id",
    "title",
    "title_without_series",
]


def connect(params_dic):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print("Connecting to the PostgreSQL database...")
        conn = psycopg2.connect(**params_dic)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit(1)
    print("Connection successful")
    return conn


ID_KEYS = ["book_id", "author_id", "publisher_id", "review_id", "series_id"]


def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx : min(ndx + n, l)]


def copy_from_stringio(conn, df, table, logger):
    """
    Here we are going save the dataframe in memory
    and use copy_from() to copy it to the table
    """
    # save dataframe to an in memory buffer
    logger.info("Inserting batch")
    buffer = StringIO()
    ddf["Team"] = data["Team"].str.split("t", n=1, expand=True)
    # df = df[book_keys]
    df.to_csv(buffer, index=False, header=False)
    buffer.seek(0)

    cursor = conn.cursor()
    try:
        # cursor.copy_from(buffer, table, sep="\t")
        cursor.copy_expert(f"""COPY {table} FROM STDIN WITH (FORMAT CSV)""", buffer)

        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logger.error("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    logger.info("copy_from_stringio() done")
    cursor.close()


def copy_from_file(conn, df, table):
    """
    Here we are going save the dataframe on disk as
    a csv file, load the csv file
    and use copy_from() to copy it to the table
    """
    # Save the dataframe to disk
    tmp_df = "./tmp_dataframe.csv"
    df.to_csv(tmp_df, index=False, header=False)
    f = open(tmp_df, "r")
    cursor = conn.cursor()
    try:
        cursor.copy_from(f, table, sep=",")
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        os.remove(tmp_df)
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("copy_from_file() done")
    cursor.close()
    os.remove(tmp_df)


DATA_FILE_MAP = {
    "books": "goodreads_books.json.gz",
    "authors": "goodreads_book_authors.json.gz",
    "works": "goodreads_book_works.json.gz",
    "series": "goodreads_book_series.json.gz",
    "interactions": "goodreads_interactions.csv",
    "reviews": "goodreads_reviews_dedup.json.gz",
    "users": "user_id_map.csv",
    "genres": "goodreads_book_genres_initial.json.gz",
}

DATA_TOTALS = {
    "books": 2360655,
    "authors": 829529,
    "works": 1521962,
    "series": 400390,
    "interactions": 228648342,
    "reads": 112131203,
    "ratings": 104551549,
    "reviews": 16219149,
    "users": 876145,
    "genres": 8,
}


class PostgresLoader:
    def __init__(
        self, data_dir, db_creds, data_file_map=DATA_FILE_MAP, totals=DATA_TOTALS
    ):
        self.data_dir = data_dir
        self.host = db_creds.host
        self.database = db_creds.database
        self.user = db_creds.user
        self.password = db_creds.password
        self.port = db_creds.port
        self.data_file_map = {k: self.data_dir / v for k, v in data_file_map.items()}
        self.logger = logging.getLogger(__name__)
        self.totals = totals

    @property
    def connection(self):
        self.logger.info("Attempting to connect to PostgreSQL database...")
        conn = None
        try:
            # connect to the PostgreSQL server
            conn = psycopg2.connect(
                host=self.host,
                database=self.database,
                user=self.user,
                password=self.password,
                port=self.port,
            )
        except (Exception, psycopg2.DatabaseError) as error:
            self.logger.error(error)
        self.logger.info("Connection successful")
        return conn

    def load_data(self, file_path: Path, table_name: str, chunk_size: int):
        file_name = file_path.name
        if file_name.endswith(".json.gz"):
            self._load_json(file_path, table_name, chunk_size)
        elif file_name.endswith(".csv"):
            self._load_csv(file_path, table_name, chunk_size)
        else:
            extension_type = Path(file_name).name.split(".", 1)[-1]
            self.logger.error(
                f"Unknown File format. Expected .json.gz or .csv. Got {extension_type}"
            )

    def load_table(self, table_name, chunk_size=100000):
        self.logger.info("Reading Data")
        self.load_data(self.data_file_map[table_name], table_name, chunk_size)
        self.logger.info("Data insertion complete")

    def load_all(self, tables=["authors", "books", "series", "users", "works"]):
        [self.load_table(i) for i in tqdm(tables)]

    def _load_json(self, file_name: Path, table_name: str, chunk_size: int):
        data = []
        with gzip.open(file_name) as fin:
            for idx, l in tqdm(enumerate(fin), total=self.totals[table_name]):
                d = json.loads(l)
                data.append(d)
                if idx % chunk_size == 0:
                    df = pd.DataFrame.from_records(data)
                    copy_from_stringio(self.connection, df, table_name, self.logger)
                    del df
                    data = []
            if len(data) > 0:
                df = pd.DataFrame.from_records(data)
                copy_from_stringio(
                    self.connection,
                    pd.DataFrame.from_records(data),
                    table_name,
                    self.logger,
                )
                del df

    def _load_csv(self, file_name: Path, table_name: str, chunk_size: int):
        pbar = tqdm(total=self.totals[table_name])
        for batch in pd.read_csv(file_name, chunksize=chunk_size):
            copy_from_stringio(self.connection, batch, table_name, self.logger)
            pbar.update(len(batch.index))


def main():
    postgres_creds = PostgresCredentials(
        "localhost", "bookgraph", "meta_reviewer", "books", "5400"
    )
    data_dir = Path("/Users/aanickel/Projects/recommender-systems/data/raw")
    postgres_loader = PostgresLoader(data_dir, postgres_creds)
    postgres_loader.load_table("works")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main()
