import gzip
import json
from pathlib import Path
import re
import numpy as np
import asyncio

import pandas as pd
from tqdm import tqdm


from neo4j import GraphDatabase

driver = GraphDatabase.driver("neo4j://localhost:7687", auth=("neo4j", "s3cr3t"))


ID_KEYS = ["book_id", "author_id", "publisher_id", "review_id", "series_id"]


base_path = Path("/Users/aanickel/Documents/Projects/recommender-systems")
data_path = base_path / "data" / "raw"

book_keys = [
    "book_id",
    "isbn",
    "text_reviews_count",
    "country_code",
    "language_code",
    "asin",
    "is_ebook",
    "average_rating",
    "kindle_asin",
    "similar_books",
    "description",
    "format",
    "link",
    "num_pages",
    "publication_day",
    "isbn13",
    "publication_month",
    "edition_information",
    "publication_year",
    "url",
    "image_url",
    "ratings_count",
    "work_id",
]


book_query = (
    "MERGE (b:Book { book_id: $book_id, isbn: $isbn, text_reviews_count: $text_reviews_count, country_code: $country_code, language_code: $language_code, asin: $asin, is_ebook: $is_ebook, average_rating: $average_rating, kindle_asin: $kindle_asin, similar_books: $similar_books, description: $description, format: $format, link: $link, num_pages: $num_pages, publication_day: $publication_day, isbn13: $isbn13, publication_month: $publication_month, edition_information: $edition_information, publication_year: $publication_year, url: $url, image_url: $image_url, ratings_count: $ratings_count, work_id: $work_id })"
    "RETURN b.title AS title"
)


author_query = (
    "MERGE (a:Author { name: $name, author_id: $author_id, text_reviews_count: $text_reviews_count, ratings_count: $ratings_count }) "
    "RETURN id(a)"
)


book_author_query = (
    "MATCH (b:Book { book_id: $book_id }),(a:Author { author_id: $author_id }) "
    "MERGE (a)-[r:AUTHORED {role: $role}]->(b) "
    "RETURN a.name, type(r), b.title"
)


AUTHOR_KEYS = ["name", "author_id", "text_reviews_count", "ratings_count"]


BOOK_AUTHOR_KEYS = ["author_id", "book_id", "role"]


async def worker(driver, query, q, pbar, n=10000):
    while True:
        with driver.session() as session:
            batch = [q.get_nowait()]  # block until at least 1
            q.task_done()
            try:  # add more until `q` is empty or `n` items obtained
                while len(batch) < n:
                    result = q.get_nowait()
                    q.task_done()
                    batch.append(result)
            except asyncio.QueueEmpty:
                tqdm.write("Queue Empty")
            except asyncio.TimeoutError:
                logging.warning("Timeout")
            except Exception as e:
                logging.warning(e)
            if len(batch) == 0:
                asyncio.Future.cancel("complete")
            else:
                commit_batch(session, batch, query)
            pbar.update(len(batch))


def load_queue(queue, records, keys=None):
    for l in records.itertuples(index=False, name="Interaction"):
        d = replace_keys(l._asdict(), keys=keys)
        queue.put_nowait(d)
    return queue


def commit_batch(session, data, query):
    with session.begin_transaction() as tx:
        [tx.run(query, **datum) for datum in data]
        tx.commit()


def replace_keys(d, id_keys=ID_KEYS, keys=None):
    keys = list(d.keys()) if keys is None else list(keys)
    keys_to_change = list(set(id_keys).intersection(set(keys)))
    if len(keys_to_change) > 0:
        d = {
            k: (f"{str(k)[0]}{v}" if k in keys_to_change else v)
            for k, v in d.items()
            if k in keys
        }
    return d


def batch(iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx : min(ndx + n, l)]


def load_data(file_name, query, keys=None, total=None):
    data = []
    with gzip.open(file_name) as fin:
        with driver.session() as session:
            for l in tqdm(fin, total=total):
                d = json.loads(l)
                d = replace_keys(d, keys=keys)
                data.append(d)
            data = list(batch(data, 10000))
            for _ in tqdm(range(len(data))):
                commit_batch(session, data.pop(), query)
    return "complete"


def load_relationship_data(file_name, query, keys=None, total=None):
    data = []
    with gzip.open(file_name) as fin:
        with driver.session() as session:
            for l in tqdm(fin, total=total):
                d = json.loads(l)
                for author in d["authors"]:
                    di = {
                        "author_id": author["author_id"],
                        "role": author["role"],
                        "book_id": d["book_id"],
                    }
                    di = replace_keys(di, keys=keys)
                    data.append(di)

            data = list(batch(data, 10000))[::-1]
            for _ in tqdm(range(len(data))):
                commit_batch(session, data.pop(), query)
    return "complete"


async def main(data_dir, batch_size, commit_size, threads):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("Loading data to Neo4j db")

    queue = asyncio.Queue()
    logger.info("async queue created")

    logger.info("loaded mapping csv to dictionary")

    driver = GraphDatabase.driver("neo4j://localhost:7687", auth=("neo4j", "s3cr3t"))

    pbar = tqdm(total=228648342)


def main():

    load_data(
        str(data_path / "goodreads_books.json.gz"), book_query, book_keys, total=2360655
    )

    load_data(
        str(data_path / "goodreads_book_authors.json.gz"),
        author_query,
        AUTHOR_KEYS,
        total=829529,
    )

    load_relationship_data(
        str(data_path / "goodreads_books.json.gz"),
        book_author_query,
        BOOK_AUTHOR_KEYS,
        total=2360655,
    )


if __name__ == "__main__":
    main()
