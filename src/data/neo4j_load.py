# -*- coding: utf-8 -*-
import asyncio
import concurrent.futures as cf
import logging
import os
from pathlib import Path

import click
from dotenv import find_dotenv, load_dotenv
from neo4j import GraphDatabase
import numpy as np
import pandas as pd
from tqdm import tqdm


ID_KEYS = ["book_id", "author_id", "publisher_id", "review_id", "series_id"]


def getn(q, n):
    batch = [q.get_nowait()]  # block until at least 1
    try:  # add more until `q` is empty or `n` items obtained
        while len(batch) < n:
            result = q.get_nowait()
            batch.append(result)
    except asyncio.QueueEmpty:
        logging.warning("Queue Empty")
    except asyncio.TimeoutError:
        logging.warning("Timeout")
    except Exception as e:
        logging.warning(e)
    return batch


def replace_keys(d, id_keys=ID_KEYS, keys=None):
    keys = list(d.keys()) if keys is None else list(keys)
    keys_to_change = list(set(id_keys).intersection(set(keys)))
    if len(keys_to_change) > 0:
        d = {
            k: (f"{str(k)[0]}{v}" if k in keys_to_change else v)
            for k, v in d.items()
            if k in keys
        }
    return d


async def worker(driver, query, q, pbar, n=10000):
    while True:
        with driver.session() as session:
            batch = [q.get_nowait()]  # block until at least 1
            q.task_done()
            try:  # add more until `q` is empty or `n` items obtained
                while len(batch) < n:
                    result = q.get_nowait()
                    q.task_done()
                    batch.append(result)
            except asyncio.QueueEmpty:
                tqdm.write("Queue Empty")
            except asyncio.TimeoutError:
                logging.warning("Timeout")
            except Exception as e:
                logging.warning(e)
            if len(batch) == 0:
                asyncio.Future.cancel("complete")
            else:
                commit_batch(session, batch, query)
            pbar.update(len(batch))


def commit_batch(session, data, query):
    with session.begin_transaction() as tx:
        [tx.run(query, **datum) for datum in data]
        tx.commit()


def load_queue(queue, records, keys=None):
    for l in records.itertuples(index=False, name="Interaction"):
        d = replace_keys(l._asdict(), keys=keys)
        queue.put_nowait(d)
    return queue


def load_csv_lookup(pth, key, value):
    df = pd.read_csv(pth)
    return dict(zip(df[key], df[value]))


interaction_query = (
    "MATCH (b:Book { book_id: $book_id })"
    "MERGE (u:User { user_id: $user_id })"
    "MERGE (u)-[r:REVIEWED {is_read: $is_read, rating: $rating, is_reviewed: $is_reviewed }]->(b) "
    "RETURN u.user_id, type(r), b.title"
)


# @click.command()
# @click.argument("data_dir", default=Path("/Users/aanickel/Documents/Projects/recommender-systems/data/raw"), type=click.Path(exists=True))
# @click.option('-b', '--batch_size', default=100000, type=int)
# @click.option('-c', '--commit_size', default=10000, type=int)
# @click.option('-t', '--threads', default=max(os.cpu_count() - 2, 2), type=int)
async def main(data_dir, batch_size, commit_size, threads):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("Loading data to Neo4j db")

    queue = asyncio.Queue()
    logger.info("async queue created")

    user_id_map = load_csv_lookup(
        str(data_dir / "user_id_map.csv"), "user_id_csv", "user_id"
    )
    book_id_map = load_csv_lookup(
        str(data_dir / "book_id_map.csv"), "book_id_csv", "book_id"
    )

    logger.info("loaded mapping csv to dictionary")

    driver = GraphDatabase.driver("neo4j://localhost:7687", auth=("neo4j", "s3cr3t"))

    pbar = tqdm(total=228648342)

    for idx, interactions in enumerate(
        pd.read_csv(str(data_dir / "goodreads_interactions.csv"), chunksize=batch_size)
    ):

        interactions.loc["user_id"] = np.vectorize(user_id_map.get)(user_id_map.values)
        interactions.loc["book_id"] = np.vectorize(book_id_map.get)(book_id_map.values)
        # pbar = tqdm(total=int(commit_size // batch_size))
        # logger.info(f"batch_{idx}")

        # logger.info("starting load queue")
        queue = load_queue(queue, interactions)

        tasks = [
            asyncio.create_task(
                worker(driver, interaction_query, queue, pbar, commit_size)
            )
            for _ in range(threads)
        ]
        # logger.info("waiting for joined")
        await queue.join()

        # logger.info("queue joined")

        for task in tasks:
            task.cancel()

        # logger.info("tasks cancelled")

        await asyncio.gather(*tasks, return_exceptions=True)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    data_dir = Path("/Users/aanickel/Documents/Projects/recommender-systems/data/raw")
    batch_size = 1000000
    commit_size = 10000
    threads = 6

    asyncio.run(main(data_dir, batch_size, commit_size, threads))
