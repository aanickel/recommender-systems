from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="A variety of recommender systems",
    author="Alex Nickel",
    license="MIT",
)
